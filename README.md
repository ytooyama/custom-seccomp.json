# custom-seccomp.json

[Dockerのデフォルトのseccomp](https://github.com/moby/moby/blob/master/profiles/seccomp/default.json)の設定のうち、ほとんどの場合不要であるSyscallの
「SYS_CHROOT」を無効にしたSeccomp profileです。

コンテナーをセキュアに利用するため、適切なseccompの設定をおすすめします。


## Dockerで利用する

daemon.jsonに次のような感じで追記してください。

```
{   
    "seccomp-profile": "/root/custom-seccomp.json"
}
```

## 参考

* [Seccomp security profiles for Docker](https://docs.docker.com/engine/security/seccomp/)
* [コンテナーとセキュリティーについて調べたのをまとめる](https://ytooyama.hatenadiary.jp/entry/2020/02/08/181025)